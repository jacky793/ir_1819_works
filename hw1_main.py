import pandas as pd
from scipy import stats
from statsmodels.stats.multicomp import pairwise_tukeyhsd
import matplotlib.pyplot as plt
import seaborn as sns  # statistical data visualization
import numpy as np

# Load resources
map_res = pd.read_csv('res/map_result.csv', sep=';')  # Load MAP values
rprec_res = pd.read_csv('res/rprec_result.csv', sep=';')  # Load RPrec values
p10_res = pd.read_csv('res/p10_result.csv', sep=';')  # Load P@10 values

# 1Way-Anova test for every measure (MAP, RPREC, P@10)
map_f, map_p = stats.f_oneway(map_res['bm25_all'], map_res['tf_idf_all'], map_res['bm25_nowrd'], map_res['tf_idf_none'])

rprec_f, rprec_p = stats.f_oneway(rprec_res['bm25_all'], rprec_res['tf_idf_all'], rprec_res['bm25_nowrd'],
                                  rprec_res['tf_idf_none'])

p10_f, p10_p = stats.f_oneway(p10_res['bm25_all'], p10_res['tf_idf_all'], p10_res['bm25_nowrd'], p10_res['tf_idf_none'])

# Create lists for perform Tukey test [1x200]
map_measures = map_res['bm25_all'].append(map_res['tf_idf_all']).append(map_res['bm25_nowrd']).append(
    map_res['tf_idf_none']).tolist()

rprec_measures = rprec_res['bm25_all'].append(rprec_res['tf_idf_all']).append(rprec_res['bm25_nowrd']).append(
    rprec_res['tf_idf_none']).tolist()

p10_measures = p10_res['bm25_all'].append(p10_res['tf_idf_all']).append(p10_res['bm25_nowrd']).append(
    p10_res['tf_idf_none']).tolist()

# Group of ordered names for Tukey analysis [1x200]
groups = ["bm25_all" for x in range(50)] + ["tf_idf_all" for y in range(50)] + ["bm25_nowrd" for z in range(50)] + [
    "tf_idf_none" for w in range(50)]

# AV measures value for each runs
# MAP
map_run1 = np.mean(map_res['bm25_all'])
map_run2 = np.mean(map_res['tf_idf_all'])
map_run3 = np.mean(map_res['bm25_nowrd']).round(6)
map_run4 = np.mean(map_res['tf_idf_none'])

# RPREC
rprec_run1 = np.mean(rprec_res['bm25_all'])
rprec_run2 = np.mean(rprec_res['tf_idf_all'])
rprec_run3 = np.mean(rprec_res['bm25_nowrd'])
rprec_run4 = np.mean(rprec_res['tf_idf_none'])

# P@10
p10_run1 = np.mean(p10_res['bm25_all'])
p10_run2 = np.mean(p10_res['tf_idf_all']).round(3)
p10_run3 = np.mean(p10_res['bm25_nowrd'])
p10_run4 = np.mean(p10_res['tf_idf_none'])

# Print results!
#
# # Summary of AV measures for all models used (50 topic)
print('\n', 'AV measures summary for all runs (50 topic)', '\n')
print('   Run                                |   MAP    |   RPrec  | P@10  |')
print('----------------------------------------------------------------------')
print('1) BM25: PorterStemmer + StopWords    |', map_run1, '|', rprec_run1, '|', p10_run1, '|')
print('2) TF_IDF: PorterStemmer + StopWords  |', map_run2, '|', rprec_run2, ' |', p10_run2, ' |')
print('3) BM25: Porter Stemmer w/o Stopwords |', map_run3, '|', rprec_run3, '|', p10_run3, '|')
print('4) TF_IDF: w/o Stemmer and Stopwords  |', map_run4, '|', rprec_run4, '|', p10_run4, ' |', '\n\n')

# 1) MAP
print('One-way ANOVA on MAP measures')
print('=============================')
print('F value:', map_f)
print('P value:', map_p)
print('-----------------------------')

map_tk = pairwise_tukeyhsd(map_measures, groups, 0.05)
print(map_tk, '\n')

# 2) RPREC
print('One-way ANOVA on RPREC measures')
print('===============================')
print('F value:', rprec_f)
print('P value:', rprec_p)
print('-------------------------------')

rprec_tk = pairwise_tukeyhsd(rprec_measures, groups, 0.05)
print(rprec_tk, '\n')

# 3) P@10
print('One-way ANOVA on P@10 measures')
print('==============================')
print('F value:', p10_f)
print('P value:', p10_p)
print('------------------------------')

p10_tk = pairwise_tukeyhsd(p10_measures, groups, 0.05)
print(p10_tk, '\n')

# Print Plots! For each measure print 1wayAnova & Tukey plots
# 1) MAP
sns.boxplot(x=map_measures, y=groups, palette="muted", showmeans=True)
plt.title("MAPs Distribution")
plt.xlabel("Average Precision (AP) value")
plt.ylabel("Run")
# plt.savefig("plots/MAP_distribution.png", bbox_inches='tight')
# plt.show()

_, map_ax = plt.subplots()  # take ax axis handle on which to attach the plot and for rename title.
map_tk_fig = map_tk.plot_simultaneous(ax=map_ax, xlabel="Average Precision (AP)", ylabel="Run")
map_ax.set_title('Multiple Comparisons Between All Pairs (Tukey) - MAP measures')
# plt.savefig("plots/MAP_tukey.png", bbox_inches='tight')
# map_tk_fig.show()

# 2) RPREC
sns.boxplot(x=rprec_measures, y=groups, palette='muted', showmeans=True)
plt.title("RPrecs Distribution")
plt.xlabel("RPrec value")
plt.ylabel("Run")
# plt.savefig("plots/RPrec_distribution.png", bbox_inches='tight')
# plt.show()

_, rprec_ax = plt.subplots()
rprec_tk_fig = rprec_tk.plot_simultaneous(ax=rprec_ax, xlabel="Precision Recall", ylabel="Run")
rprec_ax.set_title('Multiple Comparisons Between All Pairs (Tukey) - RPrec measures')
# plt.savefig("plots/RPrec_tukey.png", bbox_inches='tight')
# rprec_tk_fig.show()

# 3) P@10
sns.boxplot(x=p10_measures, y=groups, palette='muted', showmeans=True)
plt.title("P@10s Distribution")
plt.xlabel("P@10 value")
plt.ylabel("Run")
# plt.savefig("plots/P10_distribution.png", bbox_inches='tight')
# plt.show()

_, p10_ax = plt.subplots()
p10_tk_fig = p10_tk.plot_simultaneous(ax=p10_ax, xlabel="P@10", ylabel="Run")
p10_ax.set_title('Multiple Comparisons Between All Pairs (Tukey) - P@10 measures')
# plt.savefig("plots/P10_tukey.png", bbox_inches='tight')
# p10_tk_fig.show()
