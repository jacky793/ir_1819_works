# ir_HW1_1819

Project code and docs for homework1 of IR master course at | UNIPD

## Replicazione del lavoro (IDE: PyCharm)
***(purtroppo il corpus ed il resto degli input sono privati e perciò non ne è permessa la diffusione pubblica)***

Per replicare il progetto è necessario scaricare la repository ed effettuare qualche semplice passaggio:

1. SCARICARE la libreria IR di Terrier [da qui](http://terrier.org/download/) e scegliendo la versione 4.4;
2. SCARICARE il tool [The Unarchiver](https://theunarchiver.com/) (nella versione il per proprio S.O.) utile alla decompressione del corpus documentale indicato in relazione;
3. SCARICARE l'IDE PyCharm della JetBrains da [qui](https://www.jetbrains.com/pycharm/download/#section=mac) (PASSAGGIO RICHIESTO MA NON NECESSARIO)
4. POSIZIONARE le cartelle scaricate derivanti da repo + terrier seguendo l'esatta gerarchia indicata di seguito:

    
    ```
    User/USERNAME/Desktop
    ├── terrier-core-4.4
        ├── ANOVA_1-way
            ├── ir_HW1_1819  (la repo scaricata [ir_1819_works-master] va rinominata cosi!!!)
    ```
    
5. Fatto ciò non basta far altro che APRIRE il progetto dal menu di PyCharm che dovrebbe caricare tutte le risorse e le impostazioni dei path
6. Potrebbe servire un passaggio aggiuntivo per scaricare tutti i moduli mancanti e fare il setup della build (ma è locale e quasi automatica da IDE)
7. PROFIT(?)